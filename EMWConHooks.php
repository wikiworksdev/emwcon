<?php

class EMWConHooks {

	public static function onBeforePageDisplay( OutputPage &$out, Skin &$skin ) {
		global $wgEMWConSite;

		if ( $wgEMWConSite == 'production' ) {
			$out->addModules( 'ext.emwCon' );
		} else {
			$out->addModules( 'ext.emwCon-qa' );
		}

		return true;
	}
}


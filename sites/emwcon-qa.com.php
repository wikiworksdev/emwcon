<?php
# Protect against web entry
if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

############## Core configuration ##############

$wgSitename = "EMWCon QA";


## The URL base path to the directory containing the wiki;
## defaults for all runtime URL paths are based off of this.
## For more information on customizing the URLs
## (like /w/index.php/Page_title to /wiki/Page_title) please see:
## https://www.mediawiki.org/wiki/Manual:Short_URL
$wgScriptPath = "/emwcon-qa.com/w";

$wgDBname = "emwcon_qa";

############## Extension configuration ##############
# EMWCon
$wgEMWConSite = 'qa';

# googleAnalytics
$wgGoogleAnalyticsAccount = 'UA-xxxxxxx-B';

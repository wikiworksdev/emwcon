<?php
# Protect against web entry
if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

############## Core configuration ##############

$wgSitename = "EMWCon";


## The URL base path to the directory containing the wiki;
## defaults for all runtime URL paths are based off of this.
## For more information on customizing the URLs
## (like /w/index.php/Page_title to /wiki/Page_title) please see:
## https://www.mediawiki.org/wiki/Manual:Short_URL
$wgScriptPath = "/emwcon.com/w";

$wgDBname = "emwcon";

############## Extension configuration ##############
# EMWCon
$wgEMWConSite = 'production';

# googleAnalytics
$wgGoogleAnalyticsAccount = 'UA-xxxxxxx-x';
